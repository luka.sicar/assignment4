import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  @Output() gameStarted: EventEmitter<number> = new EventEmitter()
  gameRunning: boolean = false
  myInterval: any
  count: number = 0

  constructor() { }

  ngOnInit(): void {
  }

  startGame(): void {
    this.gameRunning = !this.gameRunning
    this.myInterval = setInterval(() => {
      this.count = this.count + 1
      this.gameStarted.emit(this.count)
    }, 1000)
  }

  stopGame(): void {
    this.gameRunning = !this.gameRunning
    clearInterval(this.myInterval)
  }
}
